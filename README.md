# Anders Jackson Emacs

This comments are in Swedish, if you want to read it in English, use
Google translate or any other translation service.

Detta är min initieringsfil för Emacs. Den är baserar på Org-mode och
speciellt Org-babel-mode.

Det innebär att man kan lägga till kod i Org-mode dokument och sedan
plocka ut dessa, detangle, till en fil som sedan kan kompileras eller
vad man nu vill göra.

Så alla ändringar skall göras i `my-init.org` och sedan skall man
exportera all Emacs elisp-kod.

För att detta skall fungera, så måste man till att början med flytta
undan de gamla initieringsfilerna för Emacs, dvs `~/.emacs` eller
`~/.emacs.d/init.el`. Sedan så är det bara att använda git och klona
detta förråd. Det kan göras med dessa rader:

    cd ~
	mv .emacs origin.emacs
	mv .emacs.el origin.emacs.el
	mv .emacs.d origin.emacs.d
	git clone https://gitlab.com/ajxn/my_emacs_init.git .emacs.d
	emacs &

Emacs kan eventuellt behöva köras igång några gånger, beroende på
beroendeproblem etc. etc.

Om det uppstår problem, så kan eventuellt radera katalogen
`~/.emacs.d/elpa/` lösa problemet. Detta gör att paket som installeras
från Emacs EPLP förråd kommer att laddas om. Det är en
"slägga"-lösning, om man inte vill veta exakt vad som är problemet.

# Lycka till

Anders Jackson
